// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "Sec1_PuzzlePlatformsGameMode.h"
#include "Sec1_PuzzlePlatformsCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASec1_PuzzlePlatformsGameMode::ASec1_PuzzlePlatformsGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
