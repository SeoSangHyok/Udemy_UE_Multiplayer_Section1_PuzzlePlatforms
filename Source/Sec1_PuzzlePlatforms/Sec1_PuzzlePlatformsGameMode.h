// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Sec1_PuzzlePlatformsGameMode.generated.h"

UCLASS(minimalapi)
class ASec1_PuzzlePlatformsGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASec1_PuzzlePlatformsGameMode();
};



