// Fill out your copyright notice in the Description page of Project Settings.

#include "PlatformTrigger.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
APlatformTrigger::APlatformTrigger()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TriggerVolume = CreateDefaultSubobject<UBoxComponent>(FName("TriggerVolume"));
	if (!ensure(nullptr != TriggerVolume))
		return;
	
	RootComponent = TriggerVolume;

	TriggerPadMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("TriggerPadMesh"));
	if (!ensure(nullptr != TriggerPadMesh))
		return;

	TriggerPadMesh->SetupAttachment(GetRootComponent());
}

// Called when the game starts or when spawned
void APlatformTrigger::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlatformTrigger::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

